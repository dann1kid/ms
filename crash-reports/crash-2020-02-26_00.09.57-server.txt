---- Minecraft Crash Report ----

WARNING: coremods are present:
  TransformerLoader (OpenComputers-MC1.12.2-1.7.5.192.jar)
  GTCELoadingPlugin (gregtech-1.12.2-1.9.0.481.jar)
  SpongeCoremod (spongeforge-1.12.2-2838-7.1.9.jar)
  TLSkinCapeHookLoader (tlskincape_1.12.2-1.4.jar)
Contact their authors BEFORE contacting forge

// You should try our sister game, Minceraft!

Time: 26.02.20 0:09
Description: Exception in server tick loop

net.minecraftforge.fml.common.LoaderExceptionModCrash: Caught exception from TLSkinCape (tlauncher_custom_cape_skin)
Caused by: java.lang.NoClassDefFoundError: net/minecraft/client/Minecraft
	at org.tlauncher.skin.cape.util.PreparedProfileManager.<init>(PreparedProfileManager.java:43)
	at org.tlauncher.skin.cape.TLSkinCape.event(TLSkinCape.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraftforge.fml.common.FMLModContainer.handleModStateEvent(FMLModContainer.java:637)
	at sun.reflect.GeneratedMethodAccessor10.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at com.google.common.eventbus.Subscriber.invokeSubscriberMethod(Subscriber.java:91)
	at com.google.common.eventbus.Subscriber$SynchronizedSubscriber.invokeSubscriberMethod(Subscriber.java:150)
	at com.google.common.eventbus.Subscriber$1.run(Subscriber.java:76)
	at com.google.common.util.concurrent.MoreExecutors$DirectExecutor.execute(MoreExecutors.java:399)
	at com.google.common.eventbus.Subscriber.dispatchEvent(Subscriber.java:71)
	at com.google.common.eventbus.Dispatcher$PerThreadQueuedDispatcher.dispatch(Dispatcher.java:116)
	at com.google.common.eventbus.EventBus.post(EventBus.java:217)
	at net.minecraftforge.fml.common.LoadController.sendEventToModContainer(LoadController.java:219)
	at net.minecraftforge.fml.common.LoadController.propogateStateMessage(LoadController.java:197)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at com.google.common.eventbus.Subscriber.invokeSubscriberMethod(Subscriber.java:91)
	at com.google.common.eventbus.Subscriber$SynchronizedSubscriber.invokeSubscriberMethod(Subscriber.java:150)
	at com.google.common.eventbus.Subscriber$1.run(Subscriber.java:76)
	at com.google.common.util.concurrent.MoreExecutors$DirectExecutor.execute(MoreExecutors.java:399)
	at com.google.common.eventbus.Subscriber.dispatchEvent(Subscriber.java:71)
	at com.google.common.eventbus.Dispatcher$PerThreadQueuedDispatcher.dispatch(Dispatcher.java:116)
	at com.google.common.eventbus.EventBus.post(EventBus.java:217)
	at net.minecraftforge.fml.common.LoadController.redirect$zza000$forgeImpl$PostEvent(LoadController.java:568)
	at net.minecraftforge.fml.common.LoadController.distributeStateMessage(LoadController.java:136)
	at net.minecraftforge.fml.common.Loader.initializeMods(Loader.java:754)
	at net.minecraftforge.fml.server.FMLServerHandler.finishServerLoading(FMLServerHandler.java:108)
	at net.minecraftforge.fml.common.FMLCommonHandler.onServerStarted(FMLCommonHandler.java:338)
	at net.minecraft.server.dedicated.DedicatedServer.func_71197_b(DedicatedServer.java:219)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:486)
	at java.lang.Thread.run(Unknown Source)
Caused by: java.lang.ClassNotFoundException: net.minecraft.client.Minecraft
	at net.minecraft.launchwrapper.LaunchClassLoader.findClass(LaunchClassLoader.java:101)
	at java.lang.ClassLoader.loadClass(Unknown Source)
	at java.lang.ClassLoader.loadClass(Unknown Source)
	... 38 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at net.minecraft.server.MinecraftServer.handler$zje000$onCrashReport(MinecraftServer.java:4718)

-- Sponge PhaseTracker --
Details:
	Phase Stack: [Empty stack]
Stacktrace:
	at net.minecraft.server.MinecraftServer.handler$zje000$onCrashReport(MinecraftServer.java:4718)
	at net.minecraft.server.MinecraftServer.func_71230_b(MinecraftServer.java:889)
	at net.minecraft.server.dedicated.DedicatedServer.func_71230_b(DedicatedServer.java:371)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:558)
	at java.lang.Thread.run(Unknown Source)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_241, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 372340040 bytes (355 MB) / 1666187264 bytes (1589 MB) up to 2863661056 bytes (2731 MB)
	JVM Flags: 3 total; -Xmx3G -XX:+AggressiveOpts -Xloggc:gc.log
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.5.2847 33 mods loaded, 33 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	Mods:

	| State | ID                         | Version           | Source                                        | Signature                                |
	|:----- |:-------------------------- |:----------------- |:--------------------------------------------- |:---------------------------------------- |
	| LCHIJ | minecraft                  | 1.12.2            | minecraft.jar                                 | None                                     |
	| LCHIJ | mcp                        | 9.42              | minecraft.jar                                 | None                                     |
	| LCHIJ | FML                        | 8.0.99.99         | forge.jar                                     | None                                     |
	| LCHIJ | forge                      | 14.23.5.2847      | forge.jar                                     | None                                     |
	| LCHIJ | spongeapi                  | 7.1.0-ea50f0c2    | spongeforge-1.12.2-2838-7.1.9.jar             | None                                     |
	| LCHIJ | sponge                     | 1.12.2-7.1.9      | spongeforge-1.12.2-2838-7.1.9.jar             | None                                     |
	| LCHIJ | spongeforge                | 1.12.2-2838-7.1.9 | spongeforge-1.12.2-2838-7.1.9.jar             | None                                     |
	| LCHIJ | opencomputers|core         | 1.7.5.192         | minecraft.jar                                 | None                                     |
	| LCHIJ | baubles                    | 1.5.1             | Baubles-Mod-1.12.jar                          | None                                     |
	| LCHIJ | bibliocraft                | 2.4.5             | BiblioCraft-Mod-1.12.2.jar                    | None                                     |
	| LCHIJ | botania                    | r1.10-352         | Botania-Mod-1.12.1.jar                        | None                                     |
	| LCHIJ | chameleon                  | 1.12-4.1.3        | Chameleon-Library-1.12.jar                    | None                                     |
	| LCHIJ | codechickenlib             | 3.2.3.358         | codechickenlib-1.12.2-3.2.3.358-universal.jar | f1850c39b2516232a2108a7bd84d1cb5df93b261 |
	| LCHIJ | extrautils2                | 1.0               | Extra-Utilities-Mod-1.12.2.jar                | None                                     |
	| LCHIJ | forgemultipartcbe          | 2.6.2.83          | ForgeMultipart-1.12.2-2.6.2.83-universal.jar  | f1850c39b2516232a2108a7bd84d1cb5df93b261 |
	| LCHIJ | microblockcbe              | 2.6.2.83          | ForgeMultipart-1.12.2-2.6.2.83-universal.jar  | None                                     |
	| LCHIJ | minecraftmultipartcbe      | 2.6.2.83          | ForgeMultipart-1.12.2-2.6.2.83-universal.jar  | None                                     |
	| LCHIJ | gregtech                   | 1.9.0.481         | gregtech-1.12.2-1.9.0.481.jar                 | None                                     |
	| LCHIJ | ic2                        | 2.8.170-ex112     | industrialcraft-2-2.8.170-ex112.jar           | de041f9f6187debbc77034a344134053277aa3b0 |
	| LCHIJ | mrtjpcore                  | 2.1.4.43          | MrTJPCore-1.12.2-2.1.4.43-universal (1).jar   | None                                     |
	| LCHIJ | opencomputers              | 1.7.5.192         | OpenComputers-MC1.12.2-1.7.5.192.jar          | None                                     |
	| LCHIJ | projectred-core            | 4.9.4.120         | ProjectRed-Base-1.12.2.jar                    | None                                     |
	| LCHIJ | projectred-compat          | 1.0               | projectred-1.12.2-4.9.4.120-compat.jar        | None                                     |
	| LCHIJ | projectred-integration     | 4.9.4.120         | projectred-1.12.2-4.9.4.120-integration.jar   | None                                     |
	| LCHIJ | projectred-transmission    | 4.9.4.120         | projectred-1.12.2-4.9.4.120-integration.jar   | None                                     |
	| LCHIJ | projectred-fabrication     | 4.9.4.120         | projectred-1.12.2-4.9.4.120-fabrication.jar   | None                                     |
	| LCHIJ | projectred-illumination    | 4.9.4.120         | projectred-1.12.2-4.9.4.120-lighting.jar      | None                                     |
	| LCHIJ | projectred-expansion       | 4.9.4.120         | projectred-1.12.2-4.9.4.120-mechanical.jar    | None                                     |
	| LCHIJ | projectred-relocation      | 4.9.4.120         | projectred-1.12.2-4.9.4.120-mechanical.jar    | None                                     |
	| LCHIJ | projectred-transportation  | 4.9.4.120         | projectred-1.12.2-4.9.4.120-mechanical.jar    | None                                     |
	| LCHIJ | projectred-exploration     | 4.9.4.120         | projectred-1.12.2-4.9.4.120-world.jar         | None                                     |
	| LCHIJ | storagedrawers             | 1.12.2-5.4.0      | Storage-Drawers-Mod-1.12.2.jar                | None                                     |
	| LCHIE | tlauncher_custom_cape_skin | 1.4               | tlskincape_1.12.2-1.4.jar                     | None                                     |


	Plugins:

	| State | ID | Version | Source | Signature |
	|:----- |:-- |:------- |:------ |:--------- |

	Loaded coremods (and transformers): 
TransformerLoader (OpenComputers-MC1.12.2-1.7.5.192.jar)
  li.cil.oc.common.asm.ClassTransformer
GTCELoadingPlugin (gregtech-1.12.2-1.9.0.481.jar)
  gregtech.common.asm.GTCETransformer
SpongeCoremod (spongeforge-1.12.2-2838-7.1.9.jar)
  org.spongepowered.common.launch.transformer.SpongeSuperclassTransformer
TLSkinCapeHookLoader (tlskincape_1.12.2-1.4.jar)
  gloomyfolken.hooklib.minecraft.PrimaryClassTransformer
	Profiler Position: N/A (disabled)
	Is Modded: Definitely; Server brand changed to 'fml,forge,sponge'
	Type: Dedicated Server (map_server.txt)